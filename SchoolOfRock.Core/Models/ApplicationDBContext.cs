﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolOfRock.Core.Models
{
    public class ApplicationDBContext : IdentityDbContext<ApplicationUser, ApplicationRole, string>
    {
        public ApplicationDBContext(DbContextOptions<ApplicationDBContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<ScheduleUser>().HasAlternateKey(m => new { m.UserId, m.ScheduleItemId });
            builder.Entity<ScheduleUser>().HasOne(m => m.ScheduleItem).WithMany(m => m.Users).HasForeignKey(m => m.ScheduleItemId);
            builder.Entity<ScheduleUser>().HasOne(m => m.User).WithMany(m => m.Tranings).HasForeignKey(m => m.UserId);
        }

        #region Таблицы данных

        public DbSet<ScheduleItem> ScheduleItems { get; set; }

        public DbSet<Traning> Tranings { get; set; }
        public DbSet<Log> Logs { get; set; }
        public DbSet<LogMessage> LogMessages { get; set; }
        #endregion

        #region Таблицы связей
        public DbSet<ScheduleUser> ScheduleUsers { get; set; }
        #endregion
    }
}
