﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SchoolOfRock.Core.Models.Enums
{
    public enum LogType
    {
        Success = 0,
        Error = 1,
        Warning = 2
    }
}
