﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SchoolOfRock.Core.Models
{
    public class Log
    {
        public Log()
        {
            CreateFrom = DateTime.Now;
        }

        [Key]
        public int Id { get; set; }
        public virtual ICollection<LogMessage> Messages { get; set; }
        public DateTime CreateFrom { get; set; }
    }
}
