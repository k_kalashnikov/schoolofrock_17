﻿using SchoolOfRock.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SchoolOfRock.Core.Models
{
    public class LogMessage
    {
        public LogMessage()
        {
            Type = LogType.Success;
        }

        [Key]
        public int Id { get; set; }
        public int LogId { get; set; }
        public Log Log { get; set; }
        public string Message { get; set; }
        public LogType Type { get; set; }
    }
}
