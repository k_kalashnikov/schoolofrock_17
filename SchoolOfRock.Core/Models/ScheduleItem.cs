﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SchoolOfRock.Core.Models
{
    public class ScheduleItem
    {
        [Key]
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int TraningId { get; set; }
        public Traning Traning { get; set; }
        public ICollection<ScheduleUser> Users { get; set; }
    }
}
