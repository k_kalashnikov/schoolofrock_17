﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SchoolOfRock.Core.Models
{
    public class ScheduleUser
    {
        [Key]
        public int Id { get; set; }
        public string UserId { get; set; }
        public int ScheduleItemId { get; set; }

        public ScheduleItem ScheduleItem { get; set; }
        public ApplicationUser User { get; set; }
    }
}
