﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore;
using SchoolOfRock.Core.Models;

namespace SchoolOfRock.Core
{
    class Program
    {
        public class MigrationsContextFactory : IDbContextFactory<ApplicationDBContext>
        {
            public ApplicationDBContext Create(DbContextFactoryOptions options)
            {
                var builder = new DbContextOptionsBuilder<ApplicationDBContext>();
                builder.UseSqlServer("Data Source=localhost;Initial Catalog=sor;Integrated Security=True", b => b.MigrationsAssembly("SchoolOfRock.Web"));
                return new ApplicationDBContext(builder.Options);
            }
        }
    }
}
