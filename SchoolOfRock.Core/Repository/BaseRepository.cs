﻿using Microsoft.EntityFrameworkCore;
using SchoolOfRock.Core.Models;
using SchoolOfRock.Core.Models.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace SchoolOfRock.Core.Repository
{
    public class BaseRepository<TModel, TPK> where TModel : class
    {
        protected ApplicationDBContext context { get; set; }
        protected DbSet<TModel> entitySet { get; set; }

        public BaseRepository(ApplicationDBContext _context)
        {

            if (_context == null)
            {
                throw new ArgumentNullException(nameof(_context), "Контекст базы данных не передан или равен NULL");
            }
            context = _context;
            entitySet = context.Set<TModel>();
        }

        public virtual Result<TModel> Add(TModel item)
        {
            try
            {
                TModel result = entitySet.Add(item).Entity;
                return new Result<TModel>(result, $"Элемент { item.GetType().Name } успешно добавлен");
            }
            catch (Exception e)
            {
                return new Result<TModel>(item, e.Message, LogType.Error, false);
            }
        }

        public virtual Result<TModel> Delete(TModel item)
        {
            return Remove(item);
        }

        public virtual Result<TModel> DeleteById(TPK id)
        {
            TModel item = GetById(id, true);
            if (item == null)
            {
                return new Result<TModel>(null, $"Элемент c id = {id} не найден", LogType.Error, false);
            }
            return Delete(item);
        }

        public virtual TModel GetById(TPK id, bool tracking = false)
        {


            return (tracking) ? GetAll().FirstOrDefault(m => m.GetType().GetProperty("Id").GetValue(m).Equals(id)) : GetAll().AsNoTracking().FirstOrDefault(m => m.GetType().GetProperty("Id").GetValue(m).Equals(id));
        }

        public virtual IQueryable<TModel> GetAllBase()
        {
            return entitySet;
        }

        public virtual IQueryable<TModel> GetAll()
        {
            return GetAllBase();
        }

        public virtual IQueryable<TModel> GetAll(Expression<Func<TModel, TPK>> orderBy)
        {
            return GetAll().OrderBy(orderBy);
        }

        public virtual IQueryable<TModel> Find(Expression<Func<TModel, bool>> predicate, bool tracking = false)
        {
            return (tracking) ? GetAll().Where(predicate) : GetAll().AsNoTracking().Where(predicate);
        }

        public Result<TModel> Update(TModel item)
        {
            try
            {
                TModel result = entitySet.Update(item).Entity;
                return new Result<TModel>(result, $"Элемент {item.GetType().Name} успешно обновлен");
            }
            catch (Exception e)
            {
                return new Result<TModel>(item, e.Message, LogType.Error, false);
            }
        }

        public Result<TModel> Remove(TModel item)
        {
            try
            {
                entitySet.Remove(item);
                return new Result<TModel>(item, $"Элемент типа {item.GetType().Name} удалён из базы");
            }
            catch (Exception e)
            {
                return new Result<TModel>(item, e.Message, LogType.Error, false);
            }
        }

        public bool IsExist(TModel item)
        {
            return (GetAll().AsNoTracking().FirstOrDefault(m => m.GetType().GetProperty("Id").GetValue(m).Equals(item.GetType().GetProperty("Id").GetValue(item))) is TModel);
        }

        public bool IsExist(TPK id)
        {
            return (GetAll().AsNoTracking().FirstOrDefault(m => m.GetType().GetProperty("Id").GetValue(m).Equals(id)) is TModel);
        }
    }
}
