﻿using Microsoft.EntityFrameworkCore;
using SchoolOfRock.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolOfRock.Core.Repository
{
    public class ScheduleRepository : BaseRepository<ScheduleItem, int>
    {
        public ScheduleRepository(ApplicationDBContext _context) : base(_context)
        {
        }

        public override IQueryable<ScheduleItem> GetAllBase()
        {
            return entitySet.Include(m => m.Traning)
                .Include(m => m.Users).ThenInclude(m => m.User);
        }
    }
}
