﻿using Microsoft.EntityFrameworkCore;
using SchoolOfRock.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolOfRock.Core.Repository
{
    public class TraningRepository : BaseRepository<Traning, int>
    {
        public TraningRepository(ApplicationDBContext _context) : base(_context)
        {
        }

        public override IQueryable<Traning> GetAllBase()
        {
            return entitySet.Include(m => m.Schedule);
        }
    }
}
