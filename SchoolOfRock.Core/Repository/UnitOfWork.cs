﻿using Microsoft.EntityFrameworkCore;
using SchoolOfRock.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace SchoolOfRock.Core.Repository
{
    public class UnitOfWork : IDisposable
    {
        public TraningRepository Tranings { get; set; }
        public UserRepository Users { get; set; }
        public ScheduleRepository Schedules { get; set; }
        public BaseRepository<ScheduleUser, int> ScheduleUsers { get; set; }
        public BaseRepository<ApplicationRole, string> Roles { get; set; }
        protected ApplicationDBContext context { get; private set; }

        public UnitOfWork(ApplicationDBContext _context)
        {
            context = _context;
            var properties = GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (var property in properties)
            {
                var instance = Activator.CreateInstance(property.PropertyType, context);
                property.SetValue(this, instance, null);
            }
        }

        public int Commit()
        {
            try
            {
                return context.SaveChanges();
            }
            catch (Exception e)
            {
                if (e is DbUpdateConcurrencyException)
                {
                    foreach (var entry in (e as DbUpdateConcurrencyException).Entries)
                    {
                        if (entry.GetType().Namespace.Equals(typeof(ApplicationUser).Namespace))
                        {
                            var databaseEntity = context.Users.AsNoTracking().Single(p => p.Id.Equals(entry.Property("Id").CurrentValue));
                            var databaseEntry = context.Entry(databaseEntity);
                            foreach (var property in entry.Metadata.GetProperties())
                            {
                                var proposedValue = entry.Property(property.Name).CurrentValue;
                                var originalValue = entry.Property(property.Name).OriginalValue;
                                var databaseValue = databaseEntry.Property(property.Name).CurrentValue;
                                entry.Property(property.Name).OriginalValue = databaseEntry.Property(property.Name).CurrentValue;
                            }
                        }
                        else
                        {
                            throw new NotSupportedException("Не знаю, как обрабатывать конфликты параллелизма для " + entry.Metadata.Name);
                        }
                    }
                    return context.SaveChanges();
                }



                throw new NotSupportedException($"Не знаю как обрабатывать данное исключение {e.Message}");

            }
        }

        public void Dispose()
        {
            context.Dispose();
        }
    }
}
