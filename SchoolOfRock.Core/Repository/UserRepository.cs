﻿using Microsoft.EntityFrameworkCore;
using SchoolOfRock.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SchoolOfRock.Core.Repository
{
    public class UserRepository : BaseRepository<ApplicationUser, string>
    {
        public UserRepository(ApplicationDBContext _context) : base(_context)
        {
        }

        public override IQueryable<ApplicationUser> GetAllBase()
        {
            return entitySet
                .Include(m => m.Tranings).ThenInclude(m => m.ScheduleItem)
                .Include(m => m.Roles);
        }
    }
}
