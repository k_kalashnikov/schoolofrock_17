﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SchoolOfRock.Core.Models;
using SchoolOfRock.Core.Repository;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using SchoolOfRock.Core.Tools;
using System.Reflection;
using SchoolOfRock.Web.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SchoolOfRock.Web.Controllers
{
    public class BaseController<TModel, TPK> : Controller where TModel : class
    {
        protected ApplicationDBContext context { get; set; }
        protected UnitOfWork unit { get; set; }
        protected BaseRepository<TModel, TPK> repository { get; set; }
        protected ModelLogger logger { get; set; }

        public BaseController(ApplicationDBContext _context)
        {
            context = _context;
            unit = new UnitOfWork(context);
            logger = new ModelLogger(context);
        }

        [HttpGet]
        public virtual IActionResult Index()
        {
            var result = repository.GetAll().ToList();
            return View(result);
        }

        [HttpGet]
        public virtual IActionResult Detail(TPK Id)
        {
            var result = repository.GetById(Id);
            if (result == null)
            {
                return NotFound();
            }
            return View(result);
        }

        [HttpGet]
        public virtual IActionResult CreateOrEdit(TPK Id, int logId = -1)
        {
            var model = repository.GetById(Id) ?? Activator.CreateInstance(typeof(TModel));
            var result = new BaseCreateOrEditModel<TModel>(model as TModel);
            ViewBag.Messages = logger.GetByOpration(logId);
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public virtual IActionResult CreateOrEdit(BaseCreateOrEditModel<TModel> _formResult)
        {
            if (ModelState.IsValid)
            {
                TModel model = _formResult.model;
                var result = repository.IsExist(model) ? repository.Update(model) : repository.Add(model);
                model = result.Object;
                unit.Commit();
                return RedirectToAction("CreateOrEdit", new { Id = model.GetType().GetProperty("Id").GetValue(model), logId = logger.Add(new List<LogMessage>() { result.Message }) });
            }
            return BadRequest(GetModelStateErrors(ModelState));
        }


        public virtual List<string> GetModelStateErrors(ModelStateDictionary modelState)
        {
            List<string> result = new List<string>();
            foreach (var itemVal in modelState.Values)
            {
                foreach (var itemError in itemVal.Errors)
                {
                    result.Add(itemError.ErrorMessage);
                }
            }
            return result;
        }
    }
}
