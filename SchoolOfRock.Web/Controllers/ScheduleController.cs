﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SchoolOfRock.Core.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SchoolOfRock.Web.Controllers
{
    public class ScheduleController : BaseController<ScheduleItem, int>
    {
        public ScheduleController(ApplicationDBContext _context) : base(_context)
        {
            repository = unit.Schedules;
        }
    }
}
