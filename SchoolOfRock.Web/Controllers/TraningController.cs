﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SchoolOfRock.Core.Models;
using SchoolOfRock.Web.Models;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SchoolOfRock.Web.Controllers
{
    public class TraningController : BaseController<Traning, int>
    {
        public TraningController(ApplicationDBContext _context) : base(_context)
        {
            repository = unit.Tranings;
        }

        [HttpGet]
        public IActionResult ScheduleEdit(int Id, int logId = -1)
        {
            var model = unit.Schedules.Find(m => m.TraningId == Id)?.ToList() ?? new List<ScheduleItem>();
            var result = new ScheduleSettingModel()
            {
                ScheduleList = model,
                TraningId = Id
            };
            ViewBag.Messages = logger.GetByOpration(logId);
            return View(result);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult ScheduleEdit(ScheduleSettingModel _formResult)
        {
            if (ModelState.IsValid)
            {
                var Messages = new List<LogMessage>();
                foreach (var item in _formResult.ScheduleList)
                {
                    Messages.Add(unit.Schedules.IsExist(item.Id)
                        ? unit.Schedules.Update(item).Message
                        : unit.Schedules.Add(item).Message);
                }
                unit.Commit();
                logger.Add(Messages);
                return RedirectToAction("Index");
            }
            return BadRequest(GetModelStateErrors(ModelState));
        }

        [HttpPost]
        public IActionResult GetScheduleForm(int index, int traningId)
        {
            var traning = repository.GetById(traningId);
            if (traning == null)
            {
                return BadRequest("Ошибка передачи данных");
            }
            var result = new ScheduleSettingModel();
            for (int i = 0; i < index; i++)
            {
                result.ScheduleList.Add(new ScheduleItem());
            }
            result.ScheduleList.Add(new ScheduleItem()
            {
                TraningId = traningId,
                Traning = traning
            });
            ViewBag.Index = index;
            return View("_ScheduleEditItem", result);
        }
    }
}
