﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolOfRock.Web.Models
{
    public class BaseCreateOrEditModel<TModel> where TModel : class
    {
        public BaseCreateOrEditModel() { }
        public BaseCreateOrEditModel(TModel _model)
        {
            model = _model;
        }

        public TModel model { get; set; }
    }
}
