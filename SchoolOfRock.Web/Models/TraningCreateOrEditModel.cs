﻿using SchoolOfRock.Core.Models;
using SchoolOfRock.Core.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SchoolOfRock.Web.Models
{
    public class TraningCreateOrEditModel : BaseCreateOrEditModel<Traning>
    {
        public TraningCreateOrEditModel()
        {

        }

        public TraningCreateOrEditModel(Traning _model, UnitOfWork _unit) : base(_model)
        {

        }
    }

    public class ScheduleSettingModel
    {
        public ScheduleSettingModel()
        {
            ScheduleList = new List<ScheduleItem>();
        }

        public List<ScheduleItem> ScheduleList { get; set; }
        public int TraningId { get; set; }
    }
}
